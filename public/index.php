<?php
/**
 * Реализовать возможность входа с паролем и логином с использованием
 * сессии для изменения отправленных данных в предыдущей задаче,
 * пароль и логин генерируются автоматически при первоначальной отправке формы.
 */

header('Content-Type: text/html; charset=UTF-8');


// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  // Массив для временного хранения сообщений пользователю.
  $messages = array();

  // В суперглобальном массиве $_COOKIE PHP хранит все имена и значения куки текущего запроса.
  // Выдаем сообщение об успешном сохранении.
  if (!empty($_COOKIE['save'])) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('save', '', 1);
    setcookie('login', '', 1);
    setcookie('pass', '', 1);
    // Выводим сообщение пользователю.
    $messages[] = 'Спасибо, результаты сохранены.';
    // Если в куках есть пароль, то выводим сообщение.
    if (!empty($_COOKIE['pass'])) {
      $messages[] = sprintf('Вы можете <a href="login.php">войти</a> с логином <strong>%s</strong>
        и паролем <strong>%s</strong> для изменения данных.',
        strip_tags($_COOKIE['login']),
        strip_tags($_COOKIE['pass']));
    }
  }

  // Складываем признак ошибок в массив.
  $errors = array();

  $errors['fio'] = !empty($_COOKIE['fio_error']);
  $errors['email'] = !empty($_COOKIE['email_error']);
  $errors['year'] = !empty($_COOKIE['year_error']);
  $errors['sex'] = !empty($_COOKIE['sex_error']);
  $errors['abilities'] = !empty($_COOKIE['abilities_error']);
  $errors['bio'] = !empty($_COOKIE['bio_error']);
  $errors['accept'] = !empty($_COOKIE['accept_error']);


  // Выдаем сообщения об ошибках.
  if (!empty($errors['fio'])) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('fio_error', '', 1);
    // Выводим сообщение.
    $messages[] = '<div class="error">Заполните имя.</div>';
  }
  // TODO: тут выдать сообщения об ошибках в других полях.

  // Складываем предыдущие значения полей в массив, если есть.
  // При этом санитизуем все данные для безопасного отображения в браузере.
  $values = array();
  $values['fio'] = empty($_COOKIE['fio_value']) ? '' : strip_tags($_COOKIE['fio_value']);
  // TODO: аналогично все поля.

  // Если нет предыдущих ошибок ввода, есть кука сессии, начали сессию и
  // ранее в сессию записан факт успешного логина.
  if (empty($errors) && !empty($_COOKIE[session_name()]) &&
      session_start() && !empty($_SESSION['login'])) {
    // TODO: загрузить данные пользователя из БД
    // и заполнить переменную $values,
    // предварительно санитизовав.
    printf('Вход с логином %s, uid %d', $_SESSION['login'], $_SESSION['uid']);
  }

  include('form.php');
}
// Иначе, если запрос был методом POST
else {
  // Проверяем ошибки.
  $errors = FALSE;
  $ability_data = ['god', 'idclip', 'levitation'];

  if (empty($_POST['fio'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('fio_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('fio_value', $_POST['fio'], time() + 365 * 24 * 60 * 60);
  }
  // email validate
  if (empty($_POST['email'])) {
    setcookie('email_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  elseif (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) ) {
    setcookie('email_error_incorrect', '1', time() + 24 * 60 * 60);
    $errors = TRUE; 
  }
  else {
    setcookie('email_value', $_POST['email'], time() + 12 * 30 * 24 * 60 * 60);
  }

  // year validate
  if (empty($_POST['year'])) {
    setcookie('year_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('year_value', $_POST['year'], time() + 12 * 30 * 24 * 60 * 60);
  }

  // sex validate
  $sex_answer = $_POST['sex'];
  if ($sex_answer != '0' and $sex_answer != '1') {
    setcookie('sex_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('sex_value', $_POST['sex'], time() + 12 * 30 * 24 * 60 * 60);
  }

  // abilities validate
  if (empty($_POST['abilities'])) {
    setcookie('abilities_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('abilities_value', $_POST['abilities'], time() + 365 * 24 * 60 * 60);
  }

  // bio validate
  if (empty($_POST['bio'])) {
    setcookie('bio_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('bio_value', $_POST['bio'], time() + 365 * 24 * 60 * 60);
  }
  // FINISH
  // accept validate
  if (!isset($_POST['accept'])) {
    setcookie('accept_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('accept_value', $_POST['accept'], time() + 365 * 24 * 60 * 60);
  }



// *************
// TODO: тут необходимо проверить правильность заполнения всех остальных полей.
// Сохранить в Cookie признаки ошибок и значения полей.
// *************

  if ($errors) {
    // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
    header('Location: index.php');
    exit();
  }
  else {
    // Удаляем Cookies с признаками ошибок.
    setcookie('fio_error', '', 100000);
    setcookie('email_error', '', 1);
    setcookie('year_error', '', 1);
    setcookie('sex_error', '', 1);
    setcookie('bio_error', '', 1);
    setcookie('accept_error', '', 1);
  }

  // Проверяем меняются ли ранее сохраненные данные или отправляются новые.
  if (!empty($_COOKIE[session_name()]) &&
      session_start() && !empty($_SESSION['login'])) {
    // TODO: перезаписать данные в БД новыми данными,
    // кроме логина и пароля.
    try {
      $stmt = $db->prepare("UPDATE app5 SET 
        fio = ?,
        email = ?,
        sex = ?,
        ability_god = ?,
        ability_idclip = ?,
        ability_levitation = ?,
        biography = ?,
        accept = ?");

      $stmt -> execute([
        $_COOKIE['fio'], 
        intval($_POST['year']),  
        $_POST['sex'], 
        $ability_insert['god'], 
        $ability_insert['idclip'], 
        $ability_insert['levitation'], 
        $_POST['bio'], 
        $_POST['accept'] ]);
    }
    catch(PDOException $e){
      print('Error : ' . $e->getMessage()); 
      exit();
    }

  }
  else {

    // Генерируем уникальный логин и пароль.
    
    $login = 'user';
    $pass = substr(md5(time()), 0, 9); // save hash

    // Сохраняем в Cookies.
    setcookie('login', $login);
    setcookie('pass', $pass);

    // Подключение к моей БД
    // $user = 'u20382';
    // $pass = '6479868';
    // $db = new PDO('mysql:host=localhost;dbname=u20382', $user, $pass, array(PDO::ATTR_PERSISTENT => true)
    
    



    // TODO: Сохранение данных формы, логина и хеш md5() пароля в базу данных.  DONE 
    // try {
    //   $stmt = $db->prepare("INSERT INTO app5 SET 
    //     login = ?, 
    //     password = ?, 
    //     fio = ?, 
    //     email = ?,
    //     year = ?, 
    //     sex = ?, 
    //     ability_god = ?, 
    //     ability_idclip = ?, 
    //     ability_levitation = ?, 
    //     biography = ?, 
    //     accept = ? ");
  
    //   $stmt -> execute([
    //     $_COOKIE['login'], 
    //     $_COOKIE['password'], 
    //     $_POST['fio'], 
    //     $_POST['email'],
    //     intval($_POST['year']),  
    //     $_POST['sex'],  
    //     $ability_insert['god'], 
    //     $ability_insert['idclip'], 
    //     $ability_insert['levitation'], 
    //     $_POST['bio'], 
    //     $_POST['accept'] ]);
    // }

    // catch(PDOException $e){
    //   print('Error : ' . $e->getMessage()); 
    //   exit();
    // }
  }

  // Сохраняем куку с признаком успешного сохранения.
  setcookie('save', '1');

  // Делаем перенаправление.
  header('Location: ./');
}
