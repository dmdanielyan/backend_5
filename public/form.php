<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="generator" content="GitLab Pages">
    <title>Login & Password [5]</title>
    <link rel="stylesheet" href="style.css">
    <style>
      body{
        background-image: url("fishes.jpg");
      }
      .center {
        border: solid white;
        background-color: #1598ed;
        padding: 4px;

      }
      p{
        color: #163c54;
        display: flex;
        align-items: center;
        justify-content: center;
      }
      select {
        align-items: center;
        justify-content: center;
      }
      
      /* Сообщения об ошибках и поля с ошибками выводим с красным бордюром. */
      .error {
      border: 2px solid red;
      }
    </style>
  </head>
  <body>
    
    <div class="center">
      
      <form action="" method="POST">
        
        <p>Ваше имя?</p>
        <input size='40' type='text' maxlength='25' name="fio" <?php if ($errors['fio']) {print 'class="error"';} ?> value="<?php print $values['fio']; ?>"> <br>
        
        <p>Ваш email?</p>
        <input size="40" name="email" type="text" maxlength="60" <?php if ($errors['email']) {print 'class="error"';} ?> value="<?php print $values['email']; ?>"><br>

        <p>Выберите год рождения?</p>
        <select name="year">
          <?php for ($i = 1900; $i < 2020; $i++){ ?>
          <option value="<?php print $i; ?>" <?= $i == $values['year'] ? 'selected' : ""?>><?= $i; ?></option>
          <?php  } ?>
        </select><br>
        
        <div <?php if ($errors['sex']) {print 'class="error"';} ?> value="<?php print $values['sex']; ?>"><p>Выберите пол?</p>
          <label class="radio">
            <input type="radio" name="sex" value='0' checked <?php if(isset($values['sex']) && $values['sex'] == '0') { echo 'checked="checked"';}?> />
            Мужской
          </label>
          <label class="radio">
            <input type="radio" name="sex" value='1' <?php if(isset($values['sex']) && $values['sex'] == '1') { echo 'checked="checked"';}?>/>
            Женский
          </label>
        </div>

        <div <?php if ($errors['abilities']) {print 'class="error"';} ?> value="<?php print $values['abilities']; ?>">
          <p>Какие у вас сверхспособности?</p>
          <select multiple name="abilities[]">
            <option name="abilities" value="god"    >бессмертие</option>
            <option name="abilities" value="idclip">прохождение сквозь стены</option>
            <option name="abilities" value="levitation">левитация</option>
          </select><br>
        </div>
        
        <p>Ваша биография?</p>
        <textarea rows="5" cols="60" name="bio"  <?php if($errors['bio']) {
        print 'class="error"';}?>><?php print $values['bio'];?></textarea>
        
        
        
        <p>Принимаю согласие</p>
        <input type="checkbox" name="accept" value='1' <?php if ($errors['accept']) {print 'class="error"';} ?> value="<?php print $values['accept']; ?>">
        
        <input type="submit" value="ok" />
        
        <?php
        if (!empty($messages)) {
        print('<div id="messages">');
              // Выводим все сообщения.
              foreach ($messages as $message) {
              print($message);
              }
        print('</div>');
        }
        ?>
      </form>
    </div>
  </body>
</html>